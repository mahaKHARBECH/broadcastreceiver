package com.maha.demo.broadcastreceiverexp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Dell on 29/01/2018.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "numberDb";
    public static final int DATABASE_VERSION = 1;
    public static final String CREATE = "create table " + ContractDatabase.TABLE_NAME +
            " (id integer primary key autoincrement,"+ContractDatabase.INCOMING_NUMBER+ " text);";
    public static final String DROP_TABLE = "drop table if exists "+ContractDatabase.TABLE_NAME;


    public DbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    // save data in database
    public void saveNumber (String number, SQLiteDatabase database) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContractDatabase.INCOMING_NUMBER, number);
        database.insert(ContractDatabase.TABLE_NAME,null,contentValues);
        Log.i("Inserted", number);
    }

    public Cursor readNumber(SQLiteDatabase database) {
        String[] projection = {"id",ContractDatabase.INCOMING_NUMBER};
        return  (database.query(ContractDatabase.TABLE_NAME,projection,null,null,null,null,null));
    }
}
